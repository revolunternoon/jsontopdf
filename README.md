xlsのテンプレートにWEB APIで取得したデータを設定してPDF出力するサンプル

開発環境
OS         :Windows 10 64bit
IDE        :Spring Tool Suite? 3 version:3.9.7 Based on Eclipse 4.10.0
Java       :Java SE Development Kit 8u201
Framework  :Spring-boot 2.1.2.RELEAS
Office     :LibreOffice 6.1.5.2
POI        :Apache POI 4.0.1

機能
JSON出力   :PDF化するデータの出力
PDF出力    :JSONを使用してPDFを作成する

使用方法
事前準備   :C:\workbook\template.xlsにテンプレートとなるxlsを保存
            C:\temp\jodconverter\spring-boot-workingdirディレクトリを作成
            LibreOfficeを起動しておく必要はなし
起動       :reportPDFプロジェクトをSpring bootアプリ起動
            http://localhost:8080にアクセス
