package com.example.reportpdf.app;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.jodconverter.office.DefaultOfficeManagerBuilder;
import org.jodconverter.office.LocalOfficeManager;
import org.jodconverter.office.OfficeManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.StreamingHttpOutputMessage.Body;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;
import org.jodconverter.DocumentConverter;
import org.jodconverter.LocalConverter;
import org.jodconverter.OfficeDocumentConverter;
import org.jodconverter.document.DocumentFormat;
import org.jodconverter.document.DocumentFormatRegistry;

@RestController
@Controller
public class HelloSpringBootWebController {
	
	@Autowired
	private DocumentConverter converter;
	
	// JSON api url
	private static final String URL = "http://localhost:8080/json";
	// template file path
	private static final String TEMPLATE = "c:/workbook/template.xls";
	// export gile path
	private static final String EXPORT = "c:/workbook/export.pdf";
		
    @RequestMapping(value="/", method=RequestMethod.GET)
    public ModelAndView index(ModelAndView mv) {
    	// index
        mv.setViewName("index");
        return mv;
    }
     
    @RequestMapping(value = "/json")
    public Person json() {
    	// Sample data
        Person result = new Person("山田 太郎","渋谷銀行","恵比寿支店","恵比寿 花子","0123456789",Calendar.getInstance().getTime(),"10020-039","STSD綿田","03-1111-2222","03-3333-4444");
        return result;
    }
    
    @RequestMapping(value = "/convert", method = RequestMethod.POST)
    public ResponseEntity<InputStreamResource> convert() throws Exception {
        // Input excel template
        InputStream excel = new FileInputStream(TEMPLATE);
        // Open workbook
        Workbook workbook = new HSSFWorkbook(excel);
         
        // Open Sheet
        Sheet sheet = workbook.getSheetAt(0);
        
    	final RestTemplate restTemplate = new RestTemplate();
        PersonDto person = restTemplate.getForObject(URL, PersonDto.class);
         
        // Set string
        for (Iterator<Row> sheetIte = sheet.iterator(); sheetIte.hasNext(); ) {
            // Get row
            Row row = sheetIte.next();
            
            for (Iterator<Cell> cellIte = row.iterator(); cellIte.hasNext();) {
                // Get cell
                Cell cell = cellIte.next();
                
                // Target of "String"
                if (cell.getCellType() != CellType.STRING) {
                    continue;
                }
                
                if (cell.getStringCellValue().equals("#NAME#")) {
                    // Convert #NAME#
                    cell.setCellValue(person.name);
                } else if (cell.getStringCellValue().equals("#BANK_NAME#")) {
                    // Convert #BANK_NAME#
                    cell.setCellValue(person.bank);
                } else if (cell.getStringCellValue().equals("#BRANCH_NAME#")) {
                    // Convert #BRANCH__NAME#
                    cell.setCellValue(person.branch);
                } else if (cell.getStringCellValue().equals("#ACCOUNT_NAME#")) {
                    // Convert #ACCOUNT_NAME#
                    cell.setCellValue(person.account);
                } else if (cell.getStringCellValue().equals("#ACCOUNT_NO#")) {
                    // Convert #ACCOUNT_NO#
                    cell.setCellValue(person.accountNo);
                } else if (cell.getStringCellValue().equals("#CREATE_DATE#")) {
                    // Convert #CREATE_DATE#
                    cell.setCellValue(person.date);
                } else if (cell.getStringCellValue().equals("#NO#")) {
                    // Convert 「#NO#
                    cell.setCellValue(person.no);
                } else if (cell.getStringCellValue().equals("#CONTACT_NAME#")) {
                    // Convert #CONTACT_NAME#
                    cell.setCellValue(person.contactName);
                } else if (cell.getStringCellValue().equals("#TEL_NO#")) {
                    // Convert #TEL_NO#
                    cell.setCellValue(person.telNo);
                } else if (cell.getStringCellValue().equals("#FAX_NO#")) {
                    // Convert #FAX_NO#
                    cell.setCellValue(person.faxNo);
                }
            }
        }
        
        // Write output stream
        ByteArrayOutputStream buff = new ByteArrayOutputStream();
        workbook.write(buff);
        workbook.close();
         
        // Prepare InputStream  when converting PDF
        ByteArrayInputStream in = new ByteArrayInputStream(buff.toByteArray());
        buff.close();
        excel.close();
         
        // Create PDF
        File out = new File(EXPORT);
         
		try {
			DocumentFormatRegistry registry = converter.getFormatRegistry();
			DocumentFormat format = registry.getFormatByExtension("xls");
			converter.convert(in).as(format).to(out).execute();
		} finally {
			in.close();
		}
		
        MediaType mediaType = MediaType.APPLICATION_PDF;
 
        InputStreamResource resource = new InputStreamResource(new FileInputStream(out));
 
        // Download response
        return ResponseEntity.ok()
                // Content-Disposition
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=" + out.getName())
                // Content-Type
                .contentType(mediaType)
                // Contet-Length
                .contentLength(out.length())
                .body(resource);
    }
}
