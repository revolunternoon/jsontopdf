package com.example.reportpdf.app;

import java.util.Date;

public class PersonDto {
	public String name;
	public String bank;
	public String branch;
	public String account;
	public String accountNo;
	public Date date;
	public String no;
	public String contactName;
	public String telNo;
	public String faxNo;
}
