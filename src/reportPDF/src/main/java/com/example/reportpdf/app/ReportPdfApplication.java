package com.example.reportpdf.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ReportPdfApplication {

	public static void main(String[] args) {
		SpringApplication.run(ReportPdfApplication.class, args);
	}

}

