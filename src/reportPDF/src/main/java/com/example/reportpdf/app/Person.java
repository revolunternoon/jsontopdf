package com.example.reportpdf.app;

import java.util.Date;

class Person extends PersonDto{

    public Person(String name, String bank, String branch, String account, String accountNo, Date date, String no, String contactName, String telNo, String faxNo) {
        super();
        this.name = name;
        this.bank = bank;
        this.branch = branch;
        this.account = account;
        this.accountNo = accountNo;
        this.date = date;
        this.no = no;
        this.contactName = contactName;
        this.telNo = telNo;
        this.faxNo = faxNo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBank() {
    	return bank;
    }
    
    public void setBank(String bank) {
    	this.bank = bank;
    }
    
    public String getBranch() {
    	return branch;
    }
    
    public void setBranch(String branch) {
    	this.branch = branch;
    }
    
    public String getAccount() {
    	return account;
    }
    
    public void setAccount(String account) {
    	this.account = account;
    }
    
    public String getAccountNo() {
    	return accountNo;
    }
    
    public void setAccountNo(String accountNo) {
    	this.accountNo = accountNo;
    }
    
    public Date getDate() {
    	return date;
    }
    
    public void setDate(Date date) {
    	this.date = date;
    }
    
    public String getNo() {
    	return no;
    }
    
    public void setNo(String no) {
    	this.no = no;
    }
    
    public String getContactName() {
    	return contactName;
    }
    
    public void setContactName(String contactName) {
    	this.contactName = contactName;
    }
    
    public String getTelNo() {
    	return telNo;
    }
    
    public void setTelNo(String telNo) {
    	this.telNo = telNo;
    }
    
    public String getFaxNo() {
    	return faxNo;
    }
    
    public void setFaxNo(String faxNo) {
    	this.faxNo = faxNo;
    }
}